<!--
This template contains various comments (like this). These should be replaced
with the content they ask for (e.g. a summary). Once done, make sure all are
removed, including this one.
-->

## Details

<!-- Provide a detailed description of the bug you encountered. -->

## Expected behaviour

<!-- Describe what you expected to happen. -->

## Actual behaviour

<!-- Describe what actually happened. -->

## System information

Operating system and version used: ADD HERE

Output of `ivm --version`

```
ADD HERE
```

Output of `inko --version`:

```
ADD HERE
```

Output of `uname -a`:

```
ADD HERE
```

Output of `cargo --version`:

```
cargo --version
```

/label ~Bug
