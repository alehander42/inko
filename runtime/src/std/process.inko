## Lightweight processes, managed by the virtual machine.
##
## Processes are lightweight, isolated tasks that can run concurrently. Each
## process has its own heap, and communication happens using message passing.
##
## Processes can be spawned using `process.spawn`, or `process.channel`. Sending
## messages can be done using `process.send`, or a `Sender` when using
## `process.channel`.
##
## # Lightweight
##
## Processes are lightweight, and quick to spawn. This allows one to spawn many
## processes without having to worry about consuming too much memory. The exact
## amount of memory necessary to spawn a process depends on the runtime. For
## example, IVM allocates memory for the heap in 32 KB blocks. This means that
## once you allocate your first object your process will have at least 32 KB of
## memory dedicated to itself. This may sound like a lot of memory, but IVM can
## fit roughly 1020 objects in 32 KB of memory. This means that you can fit
## roughly 1600 processes (each using up to 32 KB of memory) in 500 MB of
## memory.
##
## # Asynchronous communication
##
## Communication between processes is asynchronous. When sending a message the
## sender will not wait for a confirmation of any kind, instead it will just put
## the message in the receiver's mailbox and continue running. Receiving a
## message on the other hand will suspend the process if no message is
## available. This allows one to write concurrent code easily, without having to
## use callback functions of some sort.
##
## When sending a message to another process, the message will be deep copied.
## This ensures that two processes can never reference each other's memory
## directly. Because copying can be expensive, it is recommended to keep the
## size of messages to a minimum.
##
## # Garbage Collection
##
## Processes are garbage collected independently from each other. While other
## processes won't be suspended, the garbage collector may (and in case of IVM
## will) suspend the process for the duration of garbage collection.
##
## When a process terminates cleanly, its memory is cleaned up asynchronously.
## This ensures that no memory is leaked, and any file handles are closed
## automatically. Because resource cleanup happens asynchronously there is no
## guarantee as to when this will happen, and in case of a panic it might not
## happen at all.
##
## # Parallelism
##
## The virtual machine is able to execute multiple processes in parallel. The
## exact number of processes that can run in parallel depends on the number of
## OS threads used, which in turn can be configured by the user.
##
## # Time slices
##
## Each process is given a certain amount of work it can perform, before it is
## suspended. These units of work are called reductions. Each process starts
## with a given number of reductions, and will be suspended once this value
## reaches zero.
import std::conversion::(ToFloat, ToInteger, ToString)

## The ID of the pool to schedule regular processes on.
let PRIMARY_POOL = 0

## The ID of the pool to use for slow or (potentially) blocking operations.
let SECONDARY_POOL = 1

## The sending-half of a channel.
object Sender!(T) {
  def init(pid: ToInteger) {
    let @pid = pid.to_integer
  }

  ## Sends a message to the underlying process.
  ##
  ## # Examples
  ##
  ## Sending a message using a Sender:
  ##
  ##     import std::process::(self, Sender)
  ##
  ##     let sender: Sender!(Integer) = Sender.new(0)
  ##
  ##     sender.send(10) # => 10
  def send(message: T) -> T {
    ThisModule.send(pid: @pid, message: message)
  }

  ## Returns the PID of the `Receiver` that belongs to this `Sender`.
  def pid -> Integer {
    @pid
  }
}

## The receiving-half of a channel.
object Receiver!(T) {
  ## Receives a message of type `T` sent to the current process.
  ##
  ## # Examples
  ##
  ## Receiving a message using a `Sender` and `Receiver`:
  ##
  ##     import std::process::(self, Sender, Receiver)
  ##
  ##     let sender: Sender!(Integer) = Sender.new(0)
  ##     let receiver: Receiver!(Integer) = Receiver.new
  ##
  ##     sender.send(10)
  ##     receiver.receive # => 10
  def receive(timeout: ToFloat = 0.0) -> T {
    ThisModule.receive(timeout.to_float) as T
  }
}

## Returns the PID of the current process.
##
## # Examples
##
## Getting the PID of a process:
##
##     import std::process
##
##     process.current # => 0
def current -> Integer {
  _INKOC.process_current_pid
}

## Sends a message to a process, returning the message that was sent.
##
## # Examples
##
## Sending a message:
##
##     import std::process
##
##     let pid = process.spawn {
##       process.receive # => 'hello'
##     }
##
##     process.send(pid, 'hello') # => 'hello'
def send!(T)(pid: ToInteger, message: T) -> T {
  _INKOC.process_send_message(pid.to_integer, message)
}

## Receives a process message.
##
## Calling this method will block the current process until a message is
## received.
##
## The argument of this method can be used to set a timeout (in seconds).  If no
## message is received and the timeout expires this method will return `Nil`.
##
## Messages are received in the same order in which they are sent.
##
## # Examples
##
## Receiving a message:
##
##     import std::process
##
##     process.send(process.current, 'hello')
##
##     process.receive # => 'hello'
##
## Receiving a message with a timeout:
##
##     import std::process
##
##     process.receive(5) # => Nil
def receive(timeout: ToFloat = 0.0) {
  _INKOC.process_receive_message(timeout.to_float)
}

## Receives a message that matches a condition.
##
## The `condition` argument is a `Block` that takes a single argument, which is
## the message to test. If the `Block` returns `True`, then the message is
## returned by this method. If the `Block` returns `False`, the message is
## put back into the mailbox.
##
## If no messages match the condition, this method will suspend the current
## process for a brief period of time. The time the process will be suspended
## for can be controlled using the `recheck_interval` argument. This argument
## specifies the minimum suspension time in seconds.
##
## # Examples
##
## Receiving a message that matches our condition:
##
##     import std::process
##
##     process.send(pid: process.current, message: 20)
##     process.send(pid: process.current, message: 10)
##
##     let message = process.receive_if do (message) {
##       message == 10
##     }
##
##     message # => 10
##
## Receiving a message with a timeout:
##
##     import std::process
##
##     process.send(pid: process.current, message: 10)
##
##     let message = process.receive_if(timeout: 10, condition: do (message) {
##       message == 10
##     })
##
##     message # => 10
##
## When supplying both the `timeout` and `condition` arguments, it is preferred
## to specify the `timeout` argument first.
def receive_if(
  condition: do (Dynamic) -> Dynamic,
  timeout: ToFloat = 0.0,
  recheck_interval = 50,
) {
  let first_message = receive(timeout)
  let mut message = first_message

  {
    condition.call(message).if_true {
      return message
    }

    send(pid: current, message: message)

    message = receive(timeout)

    # It's possible none of the messages in our mailbox match the given
    # condition. This could lead to this process being suspended and resumed
    # many times, using a lot of CPU time.
    #
    # To prevent this from happening we'll manually suspend ourselves for a
    # brief period of time once we have checked all messages in the mailbox.
    message.equal?(first_message).if_true {
      suspend(recheck_interval)
    }
  }.loop
}

## Spawns a new process that will execute the given lambda.
##
## Processes are completely isolated and as such "self" in the lambda will refer
## to the module the lambda was created in.
##
## # Examples
##
## Spawning a process:
##
##     import std::process
##
##     process.spawn {
##       10 # => 10
##     }
def spawn(block: lambda) -> Integer {
  _INKOC.process_spawn(block, PRIMARY_POOL)
}

## Spawns a process that accepts messages of a single type.
##
## The returned `Sender` can be used to send messages of a single type to the
## spawned process. The spawned process in turn can use its instance of a
## `Receiver` to receive messages of the given type.
##
## The combination of the `Sender` and `Receiver` object allows one to
## communicate with a process in a type-safe way, similar to using a
## [Channel](https://en.wikipedia.org/wiki/Channel_(programming)) found in other
## languages, such as Go and Rust.
##
## A `Sender` can only send messages to the `Receiver`, and the `Receiver` in
## turn can only receive messages. For bi-directional communication you need to
## explicitly include the PID in the message.
##
## # Examples
##
## Creating a channel and sending a message to the receiver:
##
##     import std::process
##
##     let sender = process.channel!(Integer) lambda (receiver) {
##       receiver.receive # => 10
##     }
##
##     sender.send(10) # => 10
##
## Bi-directional communication:
##
##     import std::process
##
##     object Message {
##       def init(message: String) {
##         let @pid = process.current
##         let @message = message
##       }
##
##       def pid -> Integer {
##         @pid
##      }
##
##       def message -> String {
##         @message
##       }
##     }
##
##     let sender = process.channel!(Message) lambda (receiver) {
##       let message = receiver.receive
##
##       process.send(message.pid, 'pong')
##     }
##
##     sender.send(Message.new('ping'))
##
##     process.receive # => 'pong'
def channel!(T)(receiver: lambda (Receiver!(T))) -> Sender!(T) {
  let pid = spawn {
    let receiver_block = receive as lambda (Receiver!(T))
    let receiver = Receiver.new

    receiver_block.call(receiver)
  }

  send(pid: pid, message: receiver)

  Sender.new(pid)
}

## Executes the supplied closure in a separate OS thread pool, returning its
## result once it finishes executing.
##
## Using this method you can move (potentially) blocking (or otherwise slow)
## operations out of the primary pool, preventing threads of that pool from
## getting blocked.
##
## # Examples
##
## Running a simple operation:
##
##     import std::process
##
##     let result = process.blocking {
##       10 + 2
##     }
##
##     result # => 12
def blocking!(R)(block: do -> R) -> R {
  let moved = _INKOC.move_to_pool(SECONDARY_POOL)

  defer {
    # "moved" will be set to true the first time we try to move a process, and
    # false if it has already been moved to the target pool. For example:
    #
    #     import std::process
    #
    #     process.blocking {      # moved = true
    #       process.blocking {    # moved = false
    #         process.blocking {  # moved = false
    #           # ...
    #         }                   # still on the secondary pool
    #       }                     # still on the secondary pool
    #     }                       # now we can move back to the primary pool
    #
    # Using the `if_true` below, we ensure that we only move the process back
    # once we return from the outer most call to this method.
    moved.if_true {
      _INKOC.move_to_pool(PRIMARY_POOL)
    }
  }

  block.call
}

## Returns the status of a process as an `Integer`.
##
## The following values can be returned:
##
## * 0: The process has been scheduled.
## * 1: The process is running.
## * 2: The process has been suspended.
## * 3: The process has been suspended for garbage collection.
## * 4: The process is waiting for a message to arrive.
## * 5: The process finished execution.
##
## If a process does not exist (any more) then the status will also be `5`.
##
## # Examples
##
## Getting the status of a process:
##
##     import std::process
##
##     process.status(process.current) # => 1
def status(pid: ToInteger) -> Integer {
  _INKOC.process_status(pid.to_integer)
}

## Suspends the current process until it is rescheduled.
##
## The argument of this method can be used to set a minimum suspension time (in
## seconds). If no timeout is specified the process may be rescheduled at
## any time.
##
## # Examples
##
## Suspending a process:
##
##     import std::process
##
##     process.suspend # => Nil
##
## Suspending a process for a minimum amount of time:
##
##     import std::process
##
##     process.suspend(5) # => Nil
def suspend(timeout: ToFloat = 0.0) -> Nil {
  _INKOC.process_suspend_current(timeout.to_float)
  Nil
}

## Immediately terminates the current process.
##
## # Examples
##
## Terminating the current process:
##
##     import std::process
##     import std::stdio::stdout
##
##     stdout.print('before')
##
##     process.terminate
##
##     # This code will never run because at this point the process has been
##     # terminated.
##     stdout.print('after')
def terminate -> Nil {
  _INKOC.process_terminate_current
  Nil
}

## Registers the given block as this process' panic handler.
##
## A panic handler is a block to execute when the process panicks. Each process
## can only have a single handler, and newly registered handlers will overwrite
## any previous ones.
##
## Once the handler returns, the current process will be terminated. If the main
## process terminates, so does the entire program.
##
## The argument passed to the provided block is a `String` containing the panic
## message. Obtaining a stacktrace can be done using `std::debug.stacktrace`, as
## the call stack does not unwind before running the panick handler.
##
## # Examples
##
## Registering a panic handler:
##
##     import std::process
##     import std::stdio::stderr
##
##     process.panicking do (error) {
##       stderr.print(error)
##     }
##
##     process.panic('This will run the above panick handler')
def panicking(block: do (String)) -> Nil {
  _INKOC.process_set_panic_handler(block)
  Nil
}

## Defers execution of the provided block, executing it when the block this
## method is called from returns.
##
## Deferring the execution of a block can be usedul to clean up resources that
## do not outlive the scope they are defined in. For example, we can open a file
## and close it using a deferred block:
##
##     import std::process
##     import std::fs::file
##
##     def example {
##       let readme = try! file.read_only('README.md')
##
##       process.defer {
##         readme.close
##       }
##
##       try! readme.read_string
##     }
##
## Here the `ReadOnlyFile` object stored in the `readme` variable would be
## closed once we return from the `example` method.
##
## Deferred blocks are executed even when panicking, but a panic inside a
## deferred block will terminate the execution of said deferred block. When
## using `std::vm.exit`, any deferred blocks pending execution will be executed
## first.
##
## The order in which deferred blocks are executed is unspecified, and
## programs should not assume these blocks are executed in a fixed order.
##
## Deferred blocks are free to capture any variables from the enclosing scope,
## like any other closure. This means that the variables will have their values
## set to whatever the last assigned value was, prior to a return. Take this
## code for example:
##
##     import std::process
##     import std::stdio::stdout
##
##     let mut number = 10
##
##     process.defer {
##       stdout.print(number)
##     }
##
##     number = 20
##
## When the deferred block executes, the value of `number` will be `20`, because
## that was the value last assigned to `number` before we return from the scope
## this code is defined in. This means our deferred block will print `20` as
## well, _not_ `10`.
##
## # Examples
##
## Closing a file using a deferred block:
##
##     import std::process
##     import std::fs::file
##
##     let readme = try! file.read_only('README.md')
##
##     process.defer {
##       readme.close
##     }
def defer(block: do) -> Nil {
  _INKOC.process_add_defer_to_caller(block)
  Nil
}

## Pins the current process to the current OS thread, then runs the supplied
## `Block`.
##
## Once the `Block` returns, the process is unpinned. As long as a process is
## pinned, it will run on the same OS thread. Until a process unpins itself or
## terminates, the thread will not run any other processes.
##
## Use of this method is best avoided, unless you are interacting with foreign
## code (e.g. C code) that requires consecutive operations to be performed on
## the same OS thread.
##
## # Examples
##
## Running a block while the process is pinned to a thread:
##
##     import std::process
##
##     process.pinned {
##       10
##     } # => 10
def pinned!(T)(block: do -> T) -> T {
  let pinned = _INKOC.process_pin_thread

  # We use `process.defer` to ensure a process is unpinned even in the event of
  # a throw or a panic.
  defer {
    # "pinned" will be set to true the first time we pin a process, and false
    # if the process was already pinned. For example:
    #
    #     import std::process
    #
    #     process.pinned {      # pinned = true
    #       process.pinned {    # pinned = false
    #         process.pinned {  # pinned = false
    #           # ...
    #         }                 # still pinned
    #       }                   # still pinned
    #     }                     # unpinned
    #
    # Using the `if_true` below, we ensure that we only unpin the process once
    # we return from the outer most call to this method.
    pinned.if_true {
      _INKOC.process_unpin_thread
    }
  }

  block.call
}


## Triggers a panic, terminating the current process.
##
## A panic will result in a stack trace being displayed followed by a custom
## message. Once printed the process will terminate, executing any registered
## panic handlers. If no process specific panic handler is installed, the global
## panic handler (as registered using `std::vm.panicking`) will be executed.
##
## A panic should only be triggered as a last resort when no better alternative
## is available. For errors that may happen regularly (e.g. network timeouts)
## it's better to throw an error using the `throw` keyword.
##
## This method will never return.
##
## # Examples
##
## Triggering a panic:
##
##     import std::process
##
##     process.panic('Uh-oh, something bad happened!')
def panic(message: ToString) -> Void {
  _INKOC.panic(message.to_string)
}
