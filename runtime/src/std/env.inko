#! Inspection and manipulation of the current OS process' environment.
#!
#! This module provides methods for inspecting and manipulating environment
#! details of the current OS process, such as the environment variables.
import std::index::(Index, SetIndex)
import std::io::(Error as IOError)
import std::fs::path::(Path, ToPath)

impl Index!(String, String) for ThisModule {
  ## Returns the value of an environment variable.
  ##
  ## The return value will be `Nil` if the variable was not set.
  ##
  ## # Examples
  ##
  ## Obtaining the value of an environment variable:
  ##
  ##     import std::env
  ##
  ##     env['HOME'] # => '/home/alice'
  def [](name: String) -> ?String {
    _INKOC.env_get(name)
  }
}

impl SetIndex!(String, String) for ThisModule {
  ## Defines an environment variable.
  ##
  ## If the variable already exists it will be overwritten.
  ##
  ## The return value is always the new value of the environment variable.
  ##
  ## # Examples
  ##
  ## Setting an environment variable:
  ##
  ##     import std::env
  ##
  ##     env['HOME'] = '/home/bob'
  ##
  ##     env['HOME'] # => '/home/bob'
  def []=(name: String, value: String) -> String {
    _INKOC.env_set(name, value)
  }
}

## Removes an environment variable.
##
## # Example
##
## Removing a variable:
##
##     import std::env
##
##     env['FOO'] = 'foo'
##
##     env.remove('FOO') # => Nil
##     env['FOO']        # => Nil
def remove(variable: String) -> Nil {
  _INKOC.env_remove(variable)
}

## Returns all defined environment variables and their values.
##
## # Examples
##
## Obtaining all environment variables and their values:
##
##     import std::env
##
##     env.variables # => %[ 'HOME': '/home/alice', ... ]
def variables -> HashMap!(String, String) {
  let names = _INKOC.env_variables
  let map = %[]

  names.each do (name) {
    let value = ThisModule[name]

    value.if_true {
      map[name] = *value
    }
  }

  map
}

## Returns the path to the current user's home directory.
##
## # Examples
##
## Obtaining the home directory of a user:
##
##     import std::env
##
##     env.home_directory # => '/home/alice'
def home_directory -> ?Path {
  _INKOC.env_home_directory.to_path
}

## Returns the path to the temporary directory.
##
## # Examples
##
## Obtaining the temporary directory:
##
##     import std::env
##
##     env.temporary_directory # => '/tmp'
def temporary_directory -> Path {
  _INKOC.env_temp_directory.to_path
}

## Returns the current working directory.
##
## This method will throw if the directory could not be obtained. Possible
## causes for this could be:
##
## 1. The directory no longer exists.
## 1. You do not have the permissions to access the directory.
##
## # Examples
##
## Obtaining the current working directory:
##
##     import std::env
##
##     try! env.working_directory # => '/home/alice/example'
def working_directory !! IOError -> Path {
  try {
    _INKOC.env_get_working_directory.to_path
  } else (err) {
    throw IOError.new(err as String)
  }
}

## Changes the current working directory to the given directory.
##
## The return value is always the directory provided to this method.
##
## This method will throw if the directory could not be changed.
##
## # Examples
##
## Changing the current working directory:
##
##     import std::env
##
##     try! env.working_directory = '..' # => '..'
def working_directory=(directory: ToPath) !! IOError -> Path {
  try {
    _INKOC.env_set_working_directory(directory).to_path
  } else (err) {
    throw IOError.new(err as String)
  }
}

## Returns an `Array` containing all the commandline arguments passed to the
## current program.
##
## # Examples
##
##     import std::env
##
##     # Assuming this program is executed using `inko foo.inko first second`:
##     env.arguments # => ['first', 'second']
def arguments -> Array!(String) {
  _INKOC.env_arguments
}
